﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace KellysubaruParser
{
    class Program
    {
        static void Main(string[] args)
        {
            WebClient cli = new WebClient();
            cli.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36");
            string html = cli.DownloadString("https://www.kellysubaru.com/used-inventory/index.htm");
            Parser pars = new Parser(html);
            pars.GetItems();
            if (pars.Result.Count > 2)
                Console.Write(pars.Result[1]);
            else
                Console.Write($"Count Result = {pars.Result.Count}");

            Console.ReadKey();
        }
    }
}
