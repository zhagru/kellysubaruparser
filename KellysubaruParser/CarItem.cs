﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KellysubaruParser
{
    public class CarItem
    {
        public string VIN { get; set; }
        public decimal Price { get; set; }
        public string Cover { get; set; }
        public override string ToString()
        {
            return $"VIN: {VIN}\r\nPrice: {Price}\r\nCover: {Cover}\r\n";
        }
    }
}
