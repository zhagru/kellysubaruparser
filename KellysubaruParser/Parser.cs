﻿using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KellysubaruParser
{
    public class Parser
    {
        private string Html { get; set; }
        public List<CarItem> Result = new List<CarItem>();
        public Parser(string html)
        {
            Html = html ?? throw new ArgumentException("html");
        }
        public Parser GetItems()
        {
            var parser = new HtmlParser();
            var dom = parser.ParseDocument(Html);
            var list = dom.QuerySelectorAll("div.hproduct.auto.subaru");
            list.ToList().ForEach(x => {try { Result.Add(CarItemFromElement(x)); } catch { } });
            return this;
        }
        private CarItem CarItemFromElement(IElement el)
        {
            CarItem item = new CarItem();
            item.VIN = el.GetAttribute("data-vin");
            //item.Cover = el.QuerySelector(".photo").GetAttribute("data-src");
            item.Cover = el.QuerySelector("img").GetAttribute("data-src");
            if(string.IsNullOrEmpty(item.Cover))
                item.Cover = el.QuerySelector("img").GetAttribute("src");
            item.Price = decimal.Parse(el.QuerySelector(".internetPrice").QuerySelector(".value").Text().Replace("$",""));
            return item;
        }
    }
}
